'use strict';
/**
* Dependencies.
*/
var gulp = require('gulp'),
    util = require('gulp-util'),
    concat = require('gulp-concat'),
    minifycss = require('gulp-minify-css'),
    uglify = require('gulp-uglify'),
    nodemon = require('gulp-nodemon'),
    jshint = require('gulp-jshint'),
    imagemin = require('gulp-imagemin');

var assets = require('./assets');

// the build task to minify assets
gulp.task('minify', function(){

    // change the working directory to the public assets folder
    var gulpFileCwd = __dirname +'/public';
    process.chdir(gulpFileCwd);
    util.log('Working directory changed to', util.colors.magenta(gulpFileCwd));

    // concat and minify your css
    gulp.src(assets.development.css)
        .pipe(concat('style.min.css'))
        .pipe(minifycss())
        .pipe(gulp.dest('./css/'));

    // concat and minify your js
    // gulp.src(assets.development.js)
    //     .pipe(concat('scripts.js'))
    //     .pipe(uglify())
    //     .pipe(gulp.dest('./js/'));

    // optimize your images
    // gulp.src('./images/*')
    //     .pipe(imagemin())
    //     .pipe(gulp.dest('./images/'));

});


// lint javascript files
gulp.task('lint', function () {
    
    gulp.src('*.js')
        .pipe(jshint());
    gulp.src('./**/*.js')
        .pipe(jshint());
        
});

gulp.task('develop', function () {
    util.log(util.colors.magenta('Nodemon server started.'));

    // monitor files for changes.
    nodemon({ script: 'server.js', ext: 'html js', ignore: ['./node_modules/*'] })
        .on('restart', function () {
            util.log(util.colors.cyan('Server restarted'));
        });
});
