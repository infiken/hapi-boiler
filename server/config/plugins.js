'use strict';

module.exports = function(server) {
    // Options to pass into the 'Good' plugin
    var goodOptions = {
        subscribers: {
            console: ['request', 'log', 'error']
        }
    };
    var yarOptions = {
        cookieOptions: {
            password: '928ee4eb-8ea7-40f1-9fc4-62aca4ab99ea',
            isSecure: false
        }
    };
    // The Assets Configuaration Options
    var assetOptions = require('../../assets');

    server.pack.register([
        {
            plugin: require('good'),
            options: goodOptions
        },
        {
            plugin: require('hapi-assets'),
            options: assetOptions
        },
        {
            plugin: require('hapi-named-routes')
        },
        {
            plugin: require('hapi-cache-buster')
        },
        {
            plugin: require('crumb'),
            options: {
                key: 'csrftoken',
                restful: false
            }
        },
        {
            plugin: require('yar'),
            options: yarOptions
        },
        {
            plugin: require('scooter')
        }
    ], function(err) {
        if (err) throw err;
    });
};
