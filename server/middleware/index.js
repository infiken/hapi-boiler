'use strict';

var _   = require('lodash'),
    api = require('../api');

function getOptions(request) {
    return _.extend({}, request.payload, request.query, request.params /*, request.files || {} */);
}

module.exports = {
    requestHandler: function (apiMethod) {
        return function (request, reply) {
            var options = _.extend({}, request.payload, request.query, request.params /*, request.files || {} */),
                apiContext = {
                    session: request.session,
                    api: api
                };

            return apiMethod.call(apiContext, options).then(function (result) {
                reply(result || {});
            }, function (error) {
                var errorCode = error.errorCode || 500;
                var errorMsg = {
                    error: true,
                    errorMsg: _.isString(error) ? error : (_.isObject(error) ? error.message : 'Unknown API Error')
                };
                reply(errorCode, errorMsg);
            });
        };
    },
    getOptions: getOptions
};
