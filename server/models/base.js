'use strict';
var Db           = require('bookshelf').Db,
    _            = require('lodash'),
    Joi          = require('joi'),
    when         = require('when'),
    batchOptions = {
        size: 10
    };

// Base Model
Db.Model = Db.Model.extend({
    hasTimestamps: [ 'createdAt', 'updatedAt' ],
    initialize: function () {
        this.on('saving', this.saving, this);
        this.on('saving', this.validate, this);
    },

    saving: function () {
         // Remove any properties which don't belong on the model
        this.attributes = this.pick(this.visible);
    },

    validate: function (model) {
        var validationRes = Joi.validate(this.attributes, this.validationSchema, {
            allowUnknown: true
        });

        if (validationRes.error !== null) {
            return when.reject({
                type: 'ValidationError',
                message: validationRes.error.details.pop().message
            });
        }
    },

    // Convert bools to ints to be consistent
    fixBools: function (attrs) {
        _.each(attrs, function (value, key) {
            if (typeof value === 'boolean') {
                attrs[key] = value ? 1 : 0;
            }
        });

        return attrs;
    },

    // Hide restricted attributed and pivot properties
    toJSON: function (options) {
        var attrs = _.extend({}, this.attributes),
            relations = this.relations;

        attrs = _.omit(attrs, this.hidden || []);

        if (options && options.shallow) {
            return attrs;
        }

        _.each(relations, function (relation, key) {
            if (key.substring(0, 7) !== '_pivot_') {
                attrs[key] = relation.toJSON ? relation.toJSON() : relation;
            }
        });

        return attrs;
    },

    format: function (attrs) {
        return this.fixBools(attrs);
    },

    sanitize: function (attr) {
        return sanitize(this.get(attr)).xss();
    },

    touch: function () {
        if (this.hasTimestamps) {
            return this.save(this.timestamp(), { patch: true });
        }
        return this;
    }

}, {

    /**
     * Naive find all
     * @param options (optional)
     */
    findAll:  function (options) {
        options = options || {};
        return Db.Collection.forge([], {model: this}).fetch(options);
    },

    browse: function () {
        return this.findAll.apply(this, arguments);
    },

    /**
     * Naive find one where args match
     * @param args
     * @param options (optional)
     */
    findOne: function (args, options) {
        options = options || {};
        return this.forge(args).fetch(options);
    },

    /**
     * Find or reject
     * @param args
     * @param options (optional)
     */
    findOrFail: function (args, options) {
        var self = this;
        options = options || {};

        return this.forge(args).fetch(options).then(function (o) {
            if (!o) {
                return when.reject({type: 'NotFound', errorCode: 404, message: 'Resource not found'});
            }

            return o;
        });
    },

    read: function () {
        return this.findOne.apply(this, arguments);
    },

    /**
     * Naive edit
     * @param editedObj
     * @param options (optional)
     */
    edit: function (editedObj, options) {
        options = options || {};
        return this.forge({id: editedObj.id}).fetch(options).then(function (foundObj) {
            return foundObj.save(editedObj, options);
        });
    },

    update: function () {
        return this.edit.apply(this, arguments);
    },

    /**
     * Naive create
     * @param newObj
     * @param options (optional)
     */
    add: function (newObj, options) {
        options = options || {};
        return this.forge(newObj).save(null, options);
    },

    create: function () {
        return this.add.apply(this, arguments);
    },

    'delete': function () {
        return this.destroy.apply(this, arguments);
    }

});

module.exports = Db;
